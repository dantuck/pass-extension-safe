#!/bin/bash

for file in "$PREFIX"/**/*.gpg; do                           
     file="${file/$PREFIX//}"
     printf "%s\n" "----------------------------------------" >> exported_passes
     printf "%s\n" "${file%.*}" >> exported_passes
     printf "%s\n" "----------------------------------------" >> exported_passes
     pass "${file%.*}" >> exported_passes
     printf "\n\n" >> exported_passes

     printf "%s" "."
done