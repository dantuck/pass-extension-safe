PREFIX ?= /usr
DESTDIR ?=
LIBDIR ?= $(PREFIX)/lib
SYSTEM_EXTENSION_DIR ?= $(LIBDIR)/password-store/extensions
MANDIR ?= $(PREFIX)/share/man
BASHCOMPDIR ?= /etc/bash_completion.d

all:
	@echo "pass-safe is a shell script and does not need compilation, it can be simply executed."
	@echo ""
	@echo "To install it try \"make install\" instead."
	@echo
	@echo "To run pass safe one needs to have some tools installed on the system:"
	@echo "     password store"

install:
	@install -v -d "$(DESTDIR)$(MANDIR)/man1"
	@install -v -m 0644 man/pass-extension-safe.1 "$(DESTDIR)$(MANDIR)/man1/pass-safe.1"
	@install -v -d "$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/"
	@install -v -m0755 src/safe.bash "$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/safe.bash"
	@install -v -d "$(DESTDIR)$(BASHCOMPDIR)/"
	@install -v -m 644 completion/pass-safe.bash.completion  "$(DESTDIR)$(BASHCOMPDIR)/pass-safe"
	@echo
	@echo "pass-safe is installed succesfully"
	@echo

uninstall:
	@rm -vrf \
		"$(DESTDIR)$(SYSTEM_EXTENSION_DIR)/safe.bash" \
		"$(DESTDIR)$(MANDIR)/man1/pass-safe.1" \
		"$(DESTDIR)$(BASHCOMPDIR)/pass-safe"

lint:
	shellcheck -s bash src/safe.bash

.PHONY: install uninstall lint
