# pass safe

An extension for the [password store](https://www.passwordstore.org/) that allows the owner of the data to export a clear text copy of their password store for the purpose of secure backup or printing. The purpose behind this is to have a way to export and save a printed copy in a safe for offline recovery in the event the owner of the password store is not longer available or access to the password store is not possible.

`pass safe` exports all content from the password store in `~/.password-store` to a file `exported_passes` in the directory the command was run.

## Installation

- Enable password-store extensions by setting ``PASSWORD_STORE_ENABLE_EXTENSIONS=true``
- ``make install``
- alternatively add `safe.bash` to your extension folder (by default at `~/.password-store/.extensions`)

## Completion

This extensions comes with the extension bash completion support added with password-store version 1.7.3

When installed, bash completion is already installed. Alternatively source `completion/pass-safe.bash.completion`

fish and zsh completion are not available, feel free to contribute.

For bash completion prior to password-store 1.7.3 see [old documentation](https://github.com/palortoff/pass-extension-tail/blob/42c6a182fd4c2b68be21af0dc6ed40fda188da12/README.md)

## Inspiration and Credits

Bash example and most of the code comes from: [StackExchange jasonwryan](https://unix.stackexchange.com/questions/170519/export-passwords-from-the-pass-password-manager#170546).

The extension format was drawn from the [palortoff: pass-extension-tail](https://github.com/palortoff/pass-extension-tail).

## Contribution

Contributions are always welcome.